﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public float speed;


    private Vector3 direction;
    private Vector3 walkingVelocity;

    public Transform fpsCamera;
    float pitch;

    private CharacterController controller;

    void Start()
    {
        //speed = 5.0f;
       
        //direction = Vector3.zero;
       
        controller = GetComponent<CharacterController>();
       
    }

    void Update()
    {

        direction.x = Input.GetAxis("Horizontal");
        direction.z = Input.GetAxis("Vertical");
       

        direction = direction.normalized;
        walkingVelocity = direction * speed;
        controller.Move(walkingVelocity * Time.deltaTime);
        if (direction != Vector3.zero)
        {
            transform.forward = direction;
            Debug.Log(direction);
        }
        float xMouse = Input.GetAxis("Mouse x") * 10f;
        transform.Rotate(0, xMouse, 0);

        pitch -= Input.GetAxis("Mouse Y") * 10f;
        pitch = Mathf.Clamp(pitch, -45f, 45f);

        Quaternion camRotation = Quaternion.Euler(pitch 0, 0,);

        fpsCamera.localRotation = camRotation;
    }
}
